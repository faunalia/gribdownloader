atm_models = ['GFS', 'ICON', 'Arpege', 'ECMWF', 'NAM_CONUS', 'NAM_CACBN', 'NAM_PACIFIC', 'ICON_EU', 'Arpege_EU', 'Arome']

atm_model_capabilities = {
    "version": "1.2.4",
    "GFS":{
        "resolution":["_p25_", "_p50_", "_1p0_",],
        "parameter": "gfs",
        "surface_data": ["G;","W;","P;","T;","I;","c;","i;","C;","H;","R;","s;","S;","Z;"],
        "altitude": ["2;","3;","4;","5;","6;","7;","8;","9;","skewt;",],
        "intv": ["3","6","12"],
        "days": ["1","2","3","4","5","6","7","8","9","10",],
        "cyc": ["last","00","06","12","18",],
        "limits": None
    },
    "ICON":{
        "resolution":["_p25_",],
        "parameter": "icon",
        "surface_data": ["G;","W;","P;","T;","I;","c;","C;","H;","R;","S;"],
        "altitude": ["3;","5;","7;","8;","skewt;",],
        "intv": ["3","6","12"],
        "days": ["1","2","3","4","5","6","7","8",],
        "cyc": ["last","00","12",],
        "limits": None
    },
    "Arpege":{
        "resolution":["_p50_",],
        "parameter": "arpege",
        "surface_data": ["G;","W;","P;","T;","i;","C;","H;","R;",],
        "altitude": ["3;","5;","7;","8;","skewt;",],
        "intv": ["3","6","12"],
        "days": ["1","2","3","4",],
        "cyc": ["last","00","12",],
        "limits": None
    },
    "ECMWF":{
        "resolution":["_p25_", "_p50_", "_1p0_",],
        "parameter": "ecmwf",
        "surface_data": ["P;",],
        "altitude": ["5;","8;",],
        "intv": ["24"],
        "days": ["1","2","3","4","5","6","7","8","9","10",],
        "cyc": ["last","00","12",],
        "limits": None
    },
    "NAM_CONUS":{
        "resolution":["_12km_",],
        "parameter": "nam_conus",
        "surface_data": ["G;","W;","P;","T;","c;","i;","r;","C;","H;","R;","S;",],
        "altitude": [],
        "intv": ["1","3","6","12"],
        "days": ["1","2","3","4",],
        "cyc": ["last","00","06","12","18",],
        "limits": [-152.879, 12.220, -49.416, 61.310]
    },
    "NAM_CACBN":{
        "resolution":["_12km_",],
        "parameter": "nam_cacbn",
        "surface_data": ["W;","P;","T;","c;","i;","r;","C;","H;","R;",],
        "altitude": [],
        "intv": ["1","3","6","12"],
        "days": ["1","2","3","4",],
        "cyc": ["last","00","06","12","18",],
        "limits": [-100.0, 0.138, -60.148, 30.054]
    },
    "NAM_PACIFIC":{
        "resolution":["_12km_",],
        "parameter": "nam_pacific",
        "surface_data": ["W;","P;","T;","c;","i;","r;","C;","H;","R;",],
        "altitude": [],
        "intv": ["1","3","6","12"],
        "days": ["1","2","3","4",],
        "cyc": ["last","00","06","12","18",],
        "limits": [-170.0, 8.133, -140.084, 32.973]
    },
    "ICON_EU":{
        "resolution":["_p06_", ],
        "parameter": "icon_eu",
        "surface_data": ["G;","W;","P;","T;","I;","c;","C;","H;","R;","S;",],
        "altitude": [],
        "intv": ["1","3","6","12"],
        "days": ["1","2","3","4","5",],
        "cyc": ["last","00","06","12","18",],
        "limits": [-23.5, 29.5, 45.0, 70.5]
    },
    "Arpege_EU":{
        "resolution": ["_p10_", ],
        "parameter": "arpege_eu",
        "surface_data": ["G;","W;","P;","T;","C;","H;","R;",],
        "altitude": [],
        "intv": ["1","3","6","12"],
        "days": ["1","2","3",],
        "cyc": ["last","00","06","12","18",],
        "limits": [-32.0, 20.0, 42.0, 72.0]
    },
    "Arome":{
        "resolution": ["_p025_",],
        "parameter": "arome",
        "surface_data": ["G;","W;","P;","T;","C;","H;","R;",],
        "altitude": [],
        "intv": ["1","3","6","12"],
        "days": ["1","2",],
        "cyc": ["last","00","06","12","18",],
        "limits": [-8, 38, 12, 53]
    },
}

surface_data_decode = {
    "W;": "Wind (10 m)",
    "G;": "Wind gust (surface)",
    "P;": "Mean sea level pressure",
    "T;": "Temperature",
    "I;": "Isoterm 0 C",
    "c;": "CAPE (surface)",
    "i;": "CIN (surface)",
    "r;": "Reflectivity (atmosphere)",
    "C;": "Cloud cover (total)",
    "H;": "Relative humidity (2m)",
    "R;": "Total precipitation",
    "s;": "Snow (snowfall possible)",
    "S;": "Snow (depth)",
    "Z;": "Frozen rain (rainfall possible)",
}

resolution_decode = {
    "_p025_": "0.025",
    "_p06_": "0.06",
    "_p10_": "0.1",
    "_12km_": "0.11",
    "_p25_": "0.25",
    "_p50_": "0.5",
    "_1p0_": "1",
}

altitude_decode = {
    "9;": "925 mb",
    "8;": "850 mb",
    "7;": "700 mb",
    "6;": "600 mb",
    "5;": "500 mb",
    "4;": "400 mb",
    "3;": "300 mb",
    "2;": "200 mb",
    "skewt;": "Skewt-T",
}

intv_decode = {
    "1": "1",
    "3": "3",
    "6": "6",
    "12": "12",
    "24": "24",
}

days_decode = {
    "1": "1",
    "2": "2",
    "3": "3",
    "4": "4",
    "5": "5",
    "6": "6",
    "7": "7",
    "8": "8",
    "9": "9",
    "10": "10",
}

cyc_decode = {
    "00": "0 hr",
    "03": "3 hr",
    "06": "6 hr",
    "12": "12 hr",
    "18": "18 hr",
    "last": "last",
}

wave_models = ["None", "WW3", "GWAM", "EWAM",]

wave_model_capabilities = {
    "None":{
        "wmdl": "none",
        "limits": None
    },
    "WW3": {
        "wmdl": "ww3_p50_",
        "limits": None
    },
    "GWAM": {
        "wmdl": "gwam_p25_",
        "limits": None
    },
    "EWAM": {
        "wmdl": "ewam_p05_",
        "limits": [-10.5, 30.0, 42.0, 66.0]
    },
}

wavedata_decode = {
    "s;": "Significant height",
    "H;D;P;": "Swell",
    "h;d;p;": "Wind waves",
}
