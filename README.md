# gribdownloader

A QGIS plugin to automatically download meteo GRIB files, thanks to [OpenGribs](http://opengribs.org/)

A part of the larger [QNavigate](https://github.com/faunalia/qnavigate/) project